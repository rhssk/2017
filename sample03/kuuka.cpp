#include <iostream>

using namespace std;

void print_cake_lvl( int width, char lvl_symbol ) {
    for (int i = 0; i < width; ++i) cout << lvl_symbol;
    cout << "\n";
}

void print_candle_lvl( int count, char lvl_symbol ) {
    for (int i = 0; i < count; ++i) {
        cout << lvl_symbol;
        if (i != count-1) cout << " ";
    }
    cout << "\n";
}

int main()
{
    int candle_count;

    cin >> candle_count;

    print_candle_lvl( candle_count, '$');
    print_candle_lvl( candle_count, '|');
    int cake_width = (2*candle_count)-1;
    print_cake_lvl( cake_width, '-');
    print_cake_lvl( cake_width, '~');
    print_cake_lvl( cake_width, '-');
}
